use std::error::Error;
use std::fs::File;
use std::io::prelude::*;

pub struct Config {
    pub query: String,
    pub filename: String,
    pub case_sensitive: bool,
}

impl Config {
    pub fn new(args: &[String], case_sensitive: bool) -> Result<Config, &'static str> {
        let mut args = args.iter();
        let query = match args.next() {
            Some(arg) => arg.to_string(),
            None => return Err("Didn't get a query string"),
        };
        let filename = match args.next() {
            Some(arg) => arg.to_string(),
            None => return Err("Didn't get a file name"),
        };

        Ok(Config {
            query,
            filename,
            case_sensitive,
        })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let mut f = File::open(config.filename)?;
    let mut contents = String::new();

    f.read_to_string(&mut contents)?;

    let results = search(&config.query, &contents, config.case_sensitive);

    for line in results {
        println!("{}", line);
    }

    Ok(())
}

pub fn search<'a>(query: &str, contents: &'a str, case_sensitive: bool) -> Vec<&'a str> {
    let query = case_normalize(query, case_sensitive);
    contents
        .lines()
        .filter(|line| case_normalize(line, case_sensitive).contains(&query))
        .collect()
}

fn case_normalize(raw_string: &str, case_sensitive: bool) -> String {
    if case_sensitive {
        String::from(raw_string)
    } else {
        raw_string.to_lowercase()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn config_from_correct_args() {
        let args = [String::from("arg1"), String::from("arg2")];
        let config = Config::new(&args, false).unwrap();

        assert_eq!(config.query, "arg1");
        assert_eq!(config.filename, "arg2");
    }

    #[test]
    fn config_from_no_args() {
        let args = [];
        match Config::new(&args, false) {
            Err(e) => assert_eq!(e, "Didn't get a query string"),
            Ok(_) => panic!("was expecting an error"),
        }
    }

    #[test]
    fn config_from_too_few_args() {
        let args = [String::from("arg1")];
        match Config::new(&args, false) {
            Err(e) => assert_eq!(e, "Didn't get a file name"),
            Ok(_) => panic!("was expecting an error"),
        }
    }

    #[test]
    fn case_sensitive_search_with_one_result() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(
            vec!["safe, fast, productive."],
            search(query, contents, true)
        );
    }

    #[test]
    fn case_sensitive_search_with_no_results() {
        let query = "Duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        let empty: Vec<String> = vec![];
        assert_eq!(empty, search(query, contents, true));
    }

    #[test]
    fn case_insensitive_search_with_one_result() {
        let query = "Duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(
            vec!["safe, fast, productive."],
            search(query, contents, false)
        );
    }

    #[test]
    fn case_insensitive_search_with_no_results() {
        let query = "pamplemousse";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        let empty: Vec<String> = vec![];
        assert_eq!(empty, search(query, contents, false));
    }
}
