extern crate getopts;
extern crate greep;

use getopts::Options;
use greep::Config;
use std::env;
use std::process;

fn main() {
    let mut opts = Options::new();
    opts.optflag(
        "i",
        "ignore-case",
        "Ignore case distinctions in both the PATTERN and the input files.",
    );
    let options = opts.parse(env::args().skip(1)).unwrap();
    let case_insensitive = options.opt_present("i") || !env::var("CASE_INSENSITIVE").is_err();

    let config = Config::new(&options.free, !case_insensitive).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    if let Err(e) = greep::run(config) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}
